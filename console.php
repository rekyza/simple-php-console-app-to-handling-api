#!/usr/bin/php

<?php

/* Basic auth git api using username and password to connect with Github platform */
$userName = '';
$password = '';
/* end auth.. */

class consoleApp
{
    public $repoName;
    public $ownerName;
    public $branchName;
    public $arguments;

    public function __construct($argv)
    {
        $this->argv = $argv;
        $this->arguments = array();
    }

    public function getParams()
    {
        foreach ($this->argv as $value) {
            if (strpos($value, '/') !== false) {
                $explodeArguments = explode('/', $value);
                $this->arguments[0] = $explodeArguments[0];
                $this->arguments[1] = $explodeArguments[1];
                if (strpos(end($this->argv), 'service') == false) {
                    $this->arguments[2] = end($this->argv);
                } else {
                    $this->arguments[2] = $this->argv[2];
                }
            }

            if (strpos($value, "-service") !== false) {
                $clean_string = preg_replace('/[^a-zA-Z0-9]/', '', $value);
                $arg = substr($clean_string, 7);
                $this->arguments[3] = $arg;
            }
        }
        return $this->arguments;
    }
}

class loadAPIGit
{
    private $userName;
    private $password;

    public function __construct($userName, $password)
    {
        $this->userName = $userName;
        $this->password = $password;
    }

    public function requestWithAuth($url){
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1');
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$this->userName:$this->password");
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        $result=curl_exec($ch);
        try{
            if (curl_errno($ch)) {
                throw new Exception("Unable to connect to the API. Check the valid of your arguments. Correct run script ex.: ./app php-fig/log master.", curl_errno($ch));
            }
            else{
                $json_decode = json_decode($result, true);
                curl_close($ch);
                return $json_decode;
            }
        }
        catch (Exception $e){
            echo $e->getMessage();
        }
    }

}
if(isset($argv)) {
    $params = new consoleApp($argv);
    $arguments = $params->getParams();
    $ownerName = $arguments[0];
    $repoName = $arguments[1];
    $branchName = $arguments[2];
    $serviceName = 'github';
    if(isset($arguments[3]))
        $serviceName = $arguments[3];
    try{
        if($serviceName == 'github'){
            $git = new loadAPIGit($userName, $password);
            $shaUrl="https://api.github.com/repos/".$ownerName."/".$repoName."/branches/".$branchName;
            $json=$git->requestWithAuth($shaUrl);
            $sha=($json['commit']['sha']);
            echo $sha;
        }
        else{
            throw new Exception("Unknown service: ".$serviceName, 404);
        }
    }
    catch (Exception $e){
        echo $e->getMessage();
    }
}
